import React from 'react';
import { Button, Drawer , GitLabLogo, CGLogo} from '@commonground/design-system';
import MapContext from "./MapContext";
import './MapInfo.css';

class MapInfo extends React.Component {
  state = {
    showDrawer: false
  }
  setShowDrawer = (v) => {
    this.setState({ showDrawer: v })
  }
  render() {
    return (
      <div className="generalInfo">
        <h4>Alle Text Labels</h4>
        <label
          key="labels"
          className="switch">
          <input type="checkbox"
            defaultChecked={this.context.labelState}
            onChange={this.context.toggleLabelState} />
          <span className="lever"></span>
        </label>
        <h4>Grenzen</h4>
        <label
          key="neighborhoods"
          className="switch">
          <input type="checkbox"
            defaultChecked={this.context.buurtState}
            onChange={this.context.toggleBuurtState} />
          <span className="lever"></span>
        </label>
        <h4>Kaart Stijl {this.context.mapTheme.name}
        </h4>
        <label
          key="mapTheme"
          className="switch">

          <input type="checkbox"
            onChange={this.context.toggleMapTheme} />
          <span className="lever"></span>

        </label>

        <br />
        <br />
        <br />

        <Button
          variant="secondary"
          key="moreInfo"
          onClick={() => this.setShowDrawer(true)}>Meer Informatie
            </Button>
        {this.state.showDrawer ? (
          <Drawer closeHandler={() => this.setShowDrawer(false)} autoFocus style={{'z-index' : 20}}>
            <Drawer.Header title="Over de kaart?" />
            <Drawer.Content>
            <h1>Over de kaart</h1>
                <p>This application was made by: </p>
                <CGLogo style={{ height: '50px' }} />
                <p>
                  <a target="blank" href="https://commonground.nl/">
                    Build @ dev.loer Commonground.nl{" "}
                  </a>
                </p>
                <p>
                  <GitLabLogo style={{ height: '50px' }} />
                  <a target="blank" href="https://gitlab.com/commondatafactory/">
                    Code @ gitlab{" "}
                  </a>
                </p>
            </Drawer.Content>
          </Drawer>
        ) : ""}
      </div>
    )
  }
};
MapInfo.contextType = MapContext;


export default MapInfo;
