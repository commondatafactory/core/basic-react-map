import React from 'react';
import mapboxgl from 'mapbox-gl';
import '../css/mapbox-gl-v1-10-0.css';
import './Map.css';
import MapContext from "./MapContext";

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.mapRef = React.createRef();
  }
  state = {
    prevContext: {}
  };

  // GENERAL MAP SETTINGS BASED ON CONTEXT API
  // Toggle ALL style layers with type symbol and text-font layout property on and off
  toggleLabels = toggle => {
    if (toggle) {
      this.context.mapTheme.layers.forEach((layer) => {
        if (layer.type === "symbol" && layer.layout["text-font"]) {
          this.map.setLayoutProperty(layer.id, "visibility", "visible")
        }
      });
    } else {
      this.context.mapTheme.layers.forEach((layer) => {
        if (layer.type === "symbol" && layer.layout["text-font"]) {
          this.map.setLayoutProperty(layer.id, "visibility", "none")
        }
      });
    };
  };
  // Toggle Neigborhood style layers on and off
  toggleBuurt = toggle => {
    toggle
      ? this.map.setLayoutProperty("boundary", "visibility", "visible")
      : this.map.setLayoutProperty("boundary", "visibility", "none");
  };

  // SET ALL MAP STYLING
  setAllMapStyling = () => {
    this.map.setStyle(this.context.mapTheme, { "diff": false });
    this.map.on("style.load", () => {
      this.toggleBuurt(this.context.buurtState);
      this.toggleLabels(this.context.labelState);
    })
  };
  makePopup = (e) => {
    // make POPUP 
    this.popup = new mapboxgl.Popup({
      closeButton: true,
      closeOnClick: true,
      closeOnMove: false
    });

    if (this.props.activeLayerKey !== "layer0") {
      let features = this.map.queryRenderedFeatures(e.point);

      // if there are features
      if (features.length >= 1) {
        let feature = features[0];
        this.popup.setLngLat(e.lngLat);
        // Fill popup content according to active attribute 

        this.popup
          .setHTML(feature.properties.class + " " + feature.properties.subclass + " " + feature.properties.rank)
          .addTo(this.map);
      }
    }
  };
  componentDidMount() {
    // Set prev Context state in state
    this.setState({ prevContext: this.context })
    // MAP
    this.map = new mapboxgl.Map({
      container: this.mapRef.current, //react-reference toevoegen
      style: this.context.mapTheme,
      zoom: this.context.mapViewState.zoom,
      center: this.context.mapViewState.center,
      hash: true,
      attributionControl: false,
      maxZoom: 22
    });

    // Map Loading
    this.map.on("render", (e) => {
      if (e) {
        this.context.isMapLoading(true)
      }
    });
    this.map.on("idle", (e) => {
      if (e) {
        this.context.isMapLoading(false)
      }
    });

    // CONTROLS
    let nav = new mapboxgl.NavigationControl();
    this.map.addControl(nav, "top-left");
    // Scale bar control
    let scale = new mapboxgl.ScaleControl({
      maxWidth: 180,
      unit: "metric"
    });
    this.map.addControl(scale);

    // Attribution control
    let attr = new mapboxgl.AttributionControl({
      compact: true,
      customAttribution:
        "Made by: <a href='https://commondatafactory.nl/'> CDF</a>"
    });
    this.map.addControl(attr);

    // Popup on click 
    this.map.on("click", e => {
      this.makePopup(e);
    });
  };

  componentDidUpdate(prevProps, prevState) {
    // GENERAL MAP SETTINGS BASED ON CONTEXT API
    // change labels on off
    if (this.state.prevContext.labelState !== this.context.labelState) {
      this.toggleLabels(this.context.labelState);
      this.setState(prevState => {
        let prevContext = { ...prevState.prevContext }
        prevContext.labelState = this.context.labelState;
        return { prevContext }
      })
    } else
      // Change buurt borders
      if (this.state.prevContext.buurtState !== this.context.buurtState) {
        this.toggleBuurt(this.context.buurtState);
        this.setState(prevState => {
          let prevContext = { ...prevState.prevContext }
          prevContext.buurtState = this.context.buurtState;
          return { prevContext }
        })
      } else
        // Cahnge map theme
        if (this.state.prevContext.mapTheme !== this.context.mapTheme) {
          this.setState(prevState => {
            let prevContext = { ...prevState.prevContext }
            prevContext.mapTheme = this.context.mapTheme;
            return { prevContext }
          }, () => this.setAllMapStyling())
        }
    // Update map center
    if (this.state.prevContext.mapViewState !== this.context.mapViewState) {
      this.map.jumpTo(this.context.mapViewState);
      this.setState(prevState => {
        let prevContext = { ...prevState.prevContext }
        prevContext.mapViewState = this.context.mapViewState;
        return { prevContext }
      })
    };
  }

  render() {
    return (
      <div className="map" ref={this.mapRef}></div>
    )
  }
}

Map.contextType = MapContext;


export default Map;
