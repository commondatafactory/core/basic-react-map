import React from "react";

// Show the list of possible results in the navigation bar, and make selected option bold
class LocationNavListItem extends React.Component {
  render() {
    return (
      <li
        className={
          this.props.cursorPosition === this.props.i
            ? "nlmaps-geocoder-result-selected"
            : "nlmaps-geocoder-result-item"
        }
        key={this.props.item.id}
        value={this.props.item.id}
        onClick={e => this.props.handleClick(this.props.item, e)}
      >
        {this.props.item.weergavenaam}
      </li>
    );
  }
}

export default LocationNavListItem;
