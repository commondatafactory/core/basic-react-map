import React from 'react';
import {GlobalStyles, defaultTheme, darkTheme, Spinner} from '@commonground/design-system';
import {ThemeProvider } from 'styled-components';
import Map from './Map';
import './App.css';
import LocationNav from './LocationNav';
import {MapContextProvider } from "./MapContext";
import MapInfo from "./MapInfo";


class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showDrawer: false,
      appTheme: defaultTheme,
    }
  };

  // Show drawer
  setShowDrawer = (v) => {
    this.setState({ showDrawer: v })
  }

  toggleMapTheme = () => {
    this.setState({
      appTheme: this.state.appTheme === darkTheme ? defaultTheme : darkTheme
    });
  };

  render() {
    return (
      <div className="app">
        <MapContextProvider>
          <ThemeProvider theme={this.state.appTheme}>
            <GlobalStyles />
            {this.context.mapLoading ? (<Spinner className="spinner" >
            </Spinner>) : ""}
            <LocationNav/>
            <Map/>
            <MapInfo/>
          </ThemeProvider>
        </MapContextProvider>
      </div>
    );
  };
};

export default App;
