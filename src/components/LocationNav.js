import React from "react";
import LocationNavListItem from "./LocationNavListItem";
import './LocationNav.css';
import MapContext from "./MapContext";

class LocationNav extends React.Component {
  state = {
    value: {
      weergavenaam: ""
    },
    resultList: [],
    cursorPosition: -1
  };

  // Compare typed text with the names of municipalities listed in Nationaal Georegister
  // Store the relevant municipalities and show them in the dropdown menu
  handleChange = e => {
    this.setState({ value: { weergavenaam: e.target.value || "" } });
    if (e.target.value.length >= 2) {
      fetch(
        `https://geodata.nationaalgeoregister.nl/locatieserver/suggest?rows=10&q=${encodeURIComponent(
          e.target.value
        )}`
      )
        .then(response => {
          return response.json();
        })
        .then(data => {
          this.setState({ resultList: data.response.docs });
        });
      return;
    }
  };

  // Retrieve the name of the chose municipality and coordinates from Nationaal Georegister
  // Change state of the map accordingly
  lookupLocation = item => {
    fetch(
      `https://geodata.nationaalgeoregister.nl/locatieserver/lookup?fl=*&id=${encodeURIComponent(
        item.id
      )}`
    )
      .then(response => (response.ok ? response : Promise.reject(response)))
      .then(response =>
        response.json().then(data => {
          let geocodeResult = data.response.docs[0];
          if (geocodeResult) {
            geocodeResult.centroide_ll = this.wktPointToGeoJson(
              geocodeResult.centroide_ll
            );
            let zoom = 0;
            switch (geocodeResult.typesortering) {
              case 1:
                zoom = 11.5
                break;
              case 2:
                zoom = 14
                break;
              case 3:
                zoom = 16
                break;
              case 4:
                zoom = 20
                break;
              default:
                zoom = 8
                break;
            }
            this.context.changeMapViewState(
              geocodeResult.centroide_ll.coordinates,
              zoom,
              0,
              0
            );
          }
          this.clearList();
        })
      );
  };

  // Below functions deal with typing, selecting and submitting in the input bar
  handleClick = (item, e) => {
    e.preventDefault();
    this.setState(
      {
        value: {
          weergavenaam: item.weergavenaam || ""
        }
      },
      () => {
        this.lookupLocation(item);
      }
    );
    this.clearList();
  };

  handleSubmit = e => {
    // Prevent refresh on pressing Enter!!!!
    e.preventDefault();
  };

  handleKeyDown = e => {
    // Prevent default on just pressing enter
    if (e.key === "Enter") e.preventDefault();
    // If the resultList exists
    if (this.state.resultList.length > 0) {
      switch (e.key) {
        case "Enter":
          // Prevent refresh on pressing Enter!!!!
          e.preventDefault();
          if (this.state.cursorPosition >= 0) {
            let item = this.state.resultList[this.state.cursorPosition];

            this.setState(
              {
                value: {
                  weergavenaam: item.weergavenaam || ""
                }
              },
              () => {
                this.lookupLocation(item);
              }
            );
          }

          break;
        case "ArrowDown":
          // Untill End of list
          if (this.state.resultList.length - 1 > this.state.cursorPosition) {
            this.setState(prevState => ({
              cursorPosition: prevState.cursorPosition + 1
            }));
          }
          break;
        case "ArrowUp":
          // Untill Beginning of list
          if (this.state.cursorPosition !== 0) {
            this.setState(prevState => ({
              cursorPosition: prevState.cursorPosition - 1
            }));
          }
          break;

        case "Backspace":
          this.clearList();
          break;
        case "Escape":
          this.clearList();
          break;
        default:
          break;
      }
    }
  };

  clearList = () => {
    this.setState({ resultList: [] });
    this.setState({ cursorPosition: -1 });
  };

  // GIS WKT Function
  wktPointToGeoJson = (wktPoint) => {
    // Make a wkt Point object
    if (!wktPoint.includes("POINT")) {
      throw TypeError("Provided WKT geometry is not a point.");
    }
    const coordinateTuple = wktPoint.split("(")[1].split(")")[0];
    const x = parseFloat(coordinateTuple.split(" ")[0]);
    const y = parseFloat(coordinateTuple.split(" ")[1]);
    return {
      type: "Point",
      coordinates: [x, y]
    };
  }

  // Render Component
  render() {
    return (
      <div className="nlmaps-geocoder-control-container location">
        <form className="nlmaps-geocoder-control-search">
          <input
            id="nlmaps-geocoder-control-input"
            placeholder="Zoek een locatie"
            type="text"
            value={this.state.value.weergavenaam}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            onSubmit={this.handleSubmit}
            autoComplete="off"
            autoFocus="off"
          ></input>
        </form>
        {this.state.resultList.length ? (
          <ul
            className="nlmaps-geocoder-result-list"
            id="nlmaps-geocoder-control-results"
          >
            {this.state.resultList.map((item, i) => (
              <LocationNavListItem
                key={item.id}
                item={item}
                i={i}
                cursorPosition={this.state.cursorPosition}
                handleClick={this.handleClick}
              />
            ))}
          </ul>
        ) : null}
      </div>
    );
  };
};

LocationNav.contextType = MapContext;

export default LocationNav;
