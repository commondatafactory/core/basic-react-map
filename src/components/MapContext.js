import React from "react";
import MapStyleDark from '../data/mapstyle_osm_dark_v2.json';
import MapStyleLight from '../data/mapstyle_osm_light_v2.json';

const MapContext = React.createContext();

class MapContextProvider extends React.Component {
    mapThemes = {
        "dark": MapStyleDark,
        "light": MapStyleLight
    }
    state = {
        mapViewState: {
            center: [4.88, 52.35],
            zoom: 16,
            bearing: 0,
            pitch: 0
        },
        filterValues: [0, 100],
        chosenMunicipality: "",
        mapLoading: true,
        labelState: true,
        buurtState: true,
        mapTheme: this.mapThemes.dark,
        drieDee: false,
        toggleDrieDee: () => {
            this.setState({
                drieDee: this.state.drieDee ? false : true,
            });
        },
        toggleMapTheme: () => {
            this.setState({
                mapTheme:
                    this.state.mapTheme === this.mapThemes.light
                        ? this.mapThemes.dark
                        : this.mapThemes.light,
            });
        },
        toggleBuurtState: () => {
            this.setState({
                buurtState: this.state.buurtState ? false : true,
            });
        },
        toggleLabelState: () => {
            this.setState({
                labelState: this.state.labelState ? false : true,
            });
        },
        setChosenMunicipality: (mun) => {
            this.setState({ chosenMunicipality: mun });
        },
        isMapLoading: (status) => {
            this.setState({ mapLoading: status });
        },
        changeMapViewState: (coords, zoom, bearing, pitch) => {
            this.setState({
                mapViewState: Object.assign({}, this.state.mapViewState, {
                    center: coords,
                    zoom: zoom,
                    bearing: bearing,
                    pitch: pitch
                })
            });
        },
        setFilter: (bounds) => {
            this.setState({ filterValues: bounds })
        }
    };
    render() {
        return (
            <MapContext.Provider
                value={{
                    mapViewState: this.state.mapViewState,
                    chosenMunicipality: this.state.chosenMunicipality,
                    mapLoading: this.state.mapLoading,
                    labelState: this.state.labelState,
                    buurtState: this.state.buurtState,
                    mapTheme: this.state.mapTheme,
                    drieDee: this.state.drieDee,
                    filterValues: this.state.filterValues,
                    toggleMapTheme: this.state.toggleMapTheme,
                    toggleBuurtState: this.state.toggleBuurtState,
                    toggleLabelState: this.state.toggleLabelState,
                    changeMapViewState: this.state.changeMapViewState,
                    setChosenMunicipality: this.state.setChosenMunicipality,
                    isMapLoading: this.state.isMapLoading,
                    toggleDrieDee: this.state.toggleDrieDee,
                    setFilter:this.state.setFilter
                }}
            >{this.props.children}
            </MapContext.Provider>
        );
    }
};

export { MapContextProvider };

export default MapContext;
